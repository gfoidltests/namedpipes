﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Runtime.InteropServices;

namespace Server
{
    [StructLayout(LayoutKind.Sequential)]
    struct Input
    {
        public int A;
        public int B;
    }

    class Program
    {
        static void Main(string[] args)
        {
            using (var pipeServer = new NamedPipeServerStream("myTestPipe", PipeDirection.InOut))
            {
                Console.WriteLine("Pipe-server created");

                Console.WriteLine("Waiting for client connection...");
                pipeServer.WaitForConnection();
                Console.WriteLine("Client connected");

                if (pipeServer.IsConnected)
                {
                    try
                    {
                        Input input = new Input { A = 3, B = 4 };
                        byte[] buffer = new byte[Marshal.SizeOf<Input>()];
                        GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                        try
                        {
                            Marshal.StructureToPtr(input, handle.AddrOfPinnedObject(), true);
                        }
                        finally
                        {
                            handle.Free();
                        }

                        pipeServer.Write(buffer, 0, buffer.Length);
                        pipeServer.Flush();

                        Console.WriteLine("Data written. Waiting for drain");
                        pipeServer.WaitForPipeDrain();

                        int sum = pipeServer.ReadByte();
                        Console.WriteLine($"sum = {sum.ToString()}");
                    }
                    catch (IOException ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Error.WriteLine(ex.Message);
                        Console.ResetColor();
                    }

                    pipeServer.Disconnect();
                }
            }
        }
    }
}