#ifndef SET_READ_MODE
//#define SET_READ_MODE
#endif

#include <iostream>
#include <Windows.h>

using namespace std;

union Input
{
    struct
    {
        int a;
        int b;
    } Data;
    char buffer[8];
};

union Output
{
    int sum;
    char buffer[4];
};

int main()
{
    cout << "Waiting for pipeserver. Hit ENTER to continue." << endl;
    cin.get();

    //cout << "Connecting to pipe...";

#ifdef __BCPLUSPLUS__
    HANDLE pipe = CreateFileA(
        "\\\\.\\pipe\\myTestPipe",
#else
    HANDLE pipe = CreateFile(
        L"\\\\.\\pipe\\myTestPipe",
#endif
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        0,
        NULL);

    if (pipe == INVALID_HANDLE_VALUE)
    {
        cerr << endl << "Failed to open pipe due to " << GetLastError() << endl;
        return EXIT_FAILURE;
    }

    //cout << "connected." << endl;

#ifdef SET_READ_MODE
    //cout << "Setting readmode...";
    if (!SetNamedPipeHandleState(pipe, PIPE_READMODE_BYTE, NULL, NULL))
    {
        cerr << endl << "SetNamedPipeHandleState to PIPE_READMODE_BYTE failed due to " << GetLastError() << endl;
        CloseHandle(pipe);
        return EXIT_FAILURE;
    }
    //cout << "set." << endl;
#endif

    //cout << "Reading data from pipe...";

    Input input;
    DWORD success;
    DWORD bytesRead;

    success = ReadFile(
        pipe,
        input.buffer,
        sizeof(Input),
        &bytesRead,
        NULL);

    if (!success)
    {
        cerr << endl << "Read " << bytesRead << " from " << sizeof(Input) << " bytes needed to read due to " << GetLastError() << endl;
        CloseHandle(pipe);
        return EXIT_FAILURE;
    }

    Output output;
    DWORD bytesWritten;
    output.sum = input.Data.a + input.Data.b;

    success = WriteFile(
        pipe,
        output.buffer,
        sizeof(Output),
        &bytesWritten,
        NULL);

#ifdef CHECK_WRITE_SUCCESS
    if (!success)
    {
        cerr << endl << "Written " << bytesWritten << " from " << sizeof(Output) << " bytes needed to write due to " << GetLastError() << endl;
        CloseHandle(pipe);
        return EXIT_FAILURE;
    }
#endif

    CloseHandle(pipe);

    return EXIT_SUCCESS;
}