﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Runtime.InteropServices;

namespace Client.net
{
    [StructLayout(LayoutKind.Sequential)]
    struct Input
    {
        public int A;
        public int B;
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting for server to start. Any key to continue");
            Console.ReadKey();

            using (var pipeClient = new NamedPipeClientStream(".", "myTestPipe", PipeDirection.InOut))
            {
                pipeClient.Connect();

                Input input;
                byte[] buffer = new byte[Marshal.SizeOf<Input>()];
                pipeClient.Read(buffer, 0, buffer.Length);

                GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                try
                {
                    input = Marshal.PtrToStructure<Input>(handle.AddrOfPinnedObject());
                }
                finally
                {
                    handle.Free();
                }

                int sum = input.A + input.B;

                pipeClient.WriteByte((byte)sum);
            }
        }
    }
}